import React, { useState } from 'react';

const Interval = () => {
  const [intervals, setIntervals] = useState([]);
  const handleClick = () => {
    const newInterval = Date.now();
    setIntervals(prevIntervals => [...prevIntervals, newInterval]);
  };
  const formatTime = timestamp => {
    const date = new Date(timestamp);
    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    const seconds = date.getSeconds().toString().padStart(2, '0');
    const milliseconds = date.getMilliseconds().toString().padStart(3, '0');
return `${hours}:${minutes}:${seconds}.${milliseconds}`;
};
return (
    <div>
    <button style={{ fontSize: 36 }} onClick={handleClick}>
     Interval time
      </button>
      <table>
    <tbody>
    {intervals.map(interval => (
    <tr key={interval}>
    <td>{formatTime(interval)}</td>
    </tr>
))}
    </tbody>
    </table>
    </div>
);};
export default Interval;
