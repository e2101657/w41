import React from 'react';
import Interval from './Interval';
function App() {
  return (
    <div className="App">
      <Interval />
    </div>
  );
}
export default App;
